// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.
//  Created by Sensai Teknoloji A.Ş.
//  Copyright © 2022 Sensai Teknoloji A.Ş. All rights reserved.
// json V4


import PackageDescription

let package = Package(
    name: "Sensai3DModelPackage",
    products: [
        .library(
            name: "Sensai3DModelPackage",
            targets: ["Sensai3DModel"]),
    ],
    dependencies: [

    ],
    targets: [
        .binaryTarget(name: "Sensai3DModel", path: "./Sources/Sensai3DModel.xcframework")
    ]
)







